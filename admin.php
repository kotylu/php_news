<?php
session_start();
            if (!isset($_SESSION['usr']))
            {
                header("Location: login.php");
                die();
            }
    require_once("dbcon.php");
    $db = new Database();
    require_once("articleRepository.php");

    $articles = new ArticleRepository($db);
    
    function prepString($string, $length)
    {
        return substr(str_pad($string, $length, " "), 0, $length);
    }

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <title>Main News</title>
    <meta charset="UTF-8">
</head>
<body>
    <nav>
        <a href="index.php">Zprávy</a>
        <a href="categories.php">Kategorie</a>
        <a href="authors.php">Autoři</a>
        <a href="admin.php">Administrace článků</a>
        <a href="add.php">Přidat článek</a>
    </nav>
    <section>
<main>
        <h1>Články</h1>
        <h4>přehled článků</h4>

            <?php
                    echo " <table>
                            <tr>
                                <td><h5>Title</h5></td>
                                <td><h5>Text</h5></td>
                                <td><h5>Date</h5></td>
                                <td><h5>Author</h5></td>
                                <td><h5>Category</h5></td>
                                <td><h5>Delete</h5></td>
                            </tr>";
                //var_dump($articles->getFull());
                foreach($articles->getFull() as $art)
                {
                    $id = $art['ID'];
                    $title = $art['Title'];
                    $date = explode(' ', $art['Date'])[0];
                    $text = prepString($art['Text'], 25);
                    $auth = $art['Author'];
                    $cat = $art['Category'];
                    $link_del = '<a href="del.php?id_art='.$id.'">delete</a>';
                    echo "<tr><td>$title</td>  <td>$text</td>  <td>$date</td>  <td>$auth</td>  <td>$cat</td> <td>$link_del</td></tr>";
                }
            ?>
</main>
    </section>
    <div class="account">
        <?php
            if (!isset($_SESSION['usr']))
            {
                echo '<a href="login.php">login</a> <br>';
                echo '<a href="signup.php">signup</a>';
            }
            else
            {
                $usr_name = $_SESSION['usr']['Mail'];
                echo '<a href="logout.php">logout</a> <br>';
                echo "$usr_name";
                
            }
        ?>
    </div>
</body>
</html>
