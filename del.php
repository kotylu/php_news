<?php
require_once("dbcon.php");
$db = new Database();
require_once("articleRepository.php");
$articles = new ArticleRepository($db);

if(empty($_GET['id_art']))
{
    header("Location: admin.php");
    die();
}
$articles->delete($_GET['id_art']);
header("Location: admin.php");
die();

?>
