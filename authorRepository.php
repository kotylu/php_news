<?php
    require_once("dbcon.php");

    class AuthorRepository
    {
        protected $db;

        public function __construct($db)
        {
            $this->db = $db;
        }
        public function getTop5()
        {
            return $this->db->select("select * from Authors limit 5;", []);
        }
        public function getAll()
        {
            return $this->db->select("select * from Authors;", []);
        }
        public function getById($id)
        {
            return $this->db->selectOne("select * from Authors where id = :id;", [':id'=>$id]);
        }
        public function getByMail($mail)
        {
            return $this->db->selectOne("select * from Authors where mail = :mail;", [':mail'=>$mail]);
        }
        public function check($mail)
        {
            $count = $this->db->selectOne("select count(*) as pocet from Authors where mail = :mail", [':mail' => $mail]);
            return $count;
        }
        public function login($mail, $passwd)
        {
            $sql = "select * from Authors where mail = :mail";
            $author = $this->db->selectOne($sql, [ ":mail" => $mail ]);

            if (!$author)
            {
                return false;
            }
            
            if (password_verify($passwd, $author['Password']))
            {
                unset($author['Password']);
                return $author;
            }

            return false;
        }
        public function signup($mail, $passwd)
        {
            $sql = " insert into Authors
               values (default, :mail, :passwdHash)"; 
            $passwdHash = password_hash($passwd, PASSWORD_DEFAULT);

            $params = [ ':mail' => $mail,
                        ':passwdHash' => $passwdHash ];

            $this->db->update($sql, $params);
        }
    }

?>
