<?php
session_start();
require_once("dbcon.php");
$db = new Database();
require_once("articleRepository.php");
require_once("authorRepository.php");
$art_rep = new articleRepository($db);
$auth_rep = new authorRepository($db);


if(empty($_POST['art_title']) and empty($_POST['art_text']) and empty($_SESSION['usr']))
{
    header("Location: articles.php");
    die();
}
$auth_id = $auth_rep->getByMail($_SESSION['usr']['Mail'])['ID'];
var_dump($auth_id);
$art_rep->insert($_POST['art_cat'], $auth_id, $_POST['art_title'], $_POST['art_text']);
header("Location: index.php");
?>
