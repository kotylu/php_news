<?php
    require_once("dbcon.php");

    class CategoryRepository
    {
        protected $db;

        public function __construct($db)
        {
            $this->db = $db;
        }
        public function getAll()
        {
            return $this->db->select("select * from Categories;", []);
        }
        public function getById($id)
        {
            return $this->db->selectOne("select * from Categories where id = :id;", [':id'=>$id]);
        }
    }

?>
