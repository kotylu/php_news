<?php 
session_start();

require_once("dbcon.php");
require_once("authorRepository.php");
$db = new Database();
$authorRep = new AuthorRepository($db);

if (isset($_POST['mail'], $_POST['passwd']))
{
    $authorRep->signup($_POST['mail'], $_POST['passwd']);
    header("Location: index.php");
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <nav>
        <a href="index.php">Zprávy</a>
        <a href="categories.php">Kategorie</a>
        <a href="authors.php">Autoři</a>
        <a href="admin.php">Administrace článků</a>
        <a href="add.php">Přidat článek</a>
    </nav>

    <form action="" method="post">
    <h2>signup</h2>
<br>
        <label for="mail">mail:<input id="" type="text" name="mail"></label>
        <br>
        <label for="passwd">password:<input id="" type="text" name="passwd"></label>
        <br>
        <input type="submit" value="login">
    </form>
    <div class="account">
        <a href="login.php">login</a>
        <a href="signup.php">signup</a>
    </div>
</body>
</html>
