<?php

    
    class Database
    {
        const HOST = "localhost";
        const DBNAME = "mydatabase";
        const USER = "newuser";
        const PASSWD = "password";

        protected $con;

        public function __construct()
        {

            $this->con = new PDO("mysql:host=".self::HOST.";dbname=".self::DBNAME
                , self::USER
                , self::PASSWD
	            , [
		        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	        ]);
        }
        
        protected function execute($query, $params = [])
        {
	        $stmt = $this->con->prepare($query);
	        $stmt->execute($params);
	        return $stmt;
        }

       	public function select($query, $params)
        {
            $stmt = $this->execute($query, $params); 
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        public function selectOne($query, $params)
        {
            $stmt = $this->execute($query, $params);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        public function insert($query, $params)
        {
            $this->execute($query, $params);
            return $this->con->lastInsertId();
        }
        public function update($query, $params)
        {
            $stmt = $this->execute($query, $params);
            return $stmt->rowCount();
        }
    }

?>
