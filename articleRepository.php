<?php
    require_once("dbcon.php");

    class ArticleRepository
    {
        protected $db;

        public function __construct($db)
        {
            $this->db = $db;
        }
        public function getTop5()
        {
            return $this->db->select("select * from Articles limit 5;", []);
        }
        public function getAll()
        {
            return $this->db->select("select * from Articles;", []);
        }
        public function getByCat($category)
        {
            return $this->db->select("select * from Articles where ID_Category = :category;", [':category' => $category]);
        }
	    public function getByAuth($author)
	    {
	        return $this->db->select("select * from Articles where ID_Author = :auth;", [':auth' => $author]);
	    }
        public function getFull()
        {
            $query = "select
                         Categories.Name as Category,
                         Authors.Mail as Author,
                         Articles.*
                     from Articles
                     inner join Categories on
                         Categories.ID = Articles.ID_Category
                     inner join Authors on
                         Authors.ID = Articles.ID_Author;";
            return $this->db->select($query, []);
            //return $query;
        }
        public function delete($id)
        {
            $this->db->update("delete from Articles where ID = :id", [':id'=>$id]);
       }


        	public function insert($id_cat, $id_auth, $title, $text)
	{
	    $this->db->insert("insert into Articles values (default, :id_auth, :id_cat, :title, :text, NOW());", [
		    ':id_auth' => $id_auth,
		    ':id_cat' => $id_cat,
		    ':title' => $title,
		    ':text' => $text
	    ]);
	}





    }

?>
