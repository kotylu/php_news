<?php
session_start();
    require_once("dbcon.php");
    $db = new Database();
    require_once("categoryRepository.php");
    $categories = new CategoryRepository($db);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <title>Main News</title>
    <meta charset="UTF-8">
</head>
<body>
    <nav>
        <a href="index.php">Zprávy</a>
        <a href="categories.php">Kategorie</a>
        <a href="authors.php">Autoři</a>
        <a href="admin.php">Administrace článků</a>
        <a href="add.php">Přidat článek</a>
    </nav>
    <section>
        <h1>Kategorie</h1>

        <main>
            <?php
                foreach($categories->getAll() as $cat)
                {
                    $id = $cat['ID'];
                    $name = $cat['Name'];
                    $link = '<a href="articles.php?id_cat='.$id.'">'.$name.'</a>';
                    echo $link;
                }
            ?>
        </main>
    </section>
    <div class="account">
        <?php
            if (!isset($_SESSION['usr']))
            {
                echo '<a href="login.php">login</a> <br>';
                echo '<a href="signup.php">signup</a>';
            }
            else
            {
                $usr_name = $_SESSION['usr']['Mail'];
                echo '<a href="logout.php">logout</a> <br>';
                echo "$usr_name";
                
            }
        ?>
    </div>
</body>
</html>
