<?php
session_start();
    require_once("dbcon.php");
    $db = new Database();
    require_once("articleRepository.php");
    require_once("categoryRepository.php");
    require_once("authorRepository.php");

    $articles = new ArticleRepository($db);
    $cats = new CategoryRepository($db);
    $auths = new AuthorRepository($db);
    
    if(isset($_GET['id_cat']))
    {
        $id_cat = $_GET['id_cat'];
        $h = "z ".$cats->getById($id_cat)['Name'];
    }
    if(isset($_GET['id_auth']))
    {
        $id_auth = $_GET['id_auth'];
        $h = "od ".$auths->getById($id_auth)['Mail'];
    }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <title>Main News</title>
    <meta charset="UTF-8">
</head>
<body>
    <nav>
        <a href="index.php">Zprávy</a>
        <a href="categories.php">Kategorie</a>
        <a href="authors.php">Autoři</a>
        <a href="admin.php">Administrace článků</a>
        <a href="add.php">Přidat článek</a>
    </nav>
    <section>
        <h1>Články</h1>
        <?php
            echo "<h4>Nejnovější zprávy $h</h4>"
        ?>

        <main>
            <?php
    		    function display($art)
		        {
                    $title = $art['Title'];
                    $date = $art['Date'];
                    $text = $art['Text'];
                    echo "<article>";
                    echo "<h3>$title</h3>";
                    echo "<h6>$date</h6>";
                    echo "<section>$text</section>" ;
                    echo "</article>";
		        }
    		    if(isset($_GET['id_cat']))
		        {
                    foreach($articles->getByCat($id_cat) as $art)
                    {
			            display($art);
                    }
		        }
		        if(isset($_GET['id_auth']))
		        {
		            foreach($articles->getByAuth($id_auth) as $art)
		            {
			            display($art);
		            }
		        }
            ?>
        </main>
    </section>
    <div class="account">
        <?php
            if (!isset($_SESSION['usr']))
            {
                echo '<a href="login.php">login</a> <br>';
                echo '<a href="signup.php">signup</a>';
            }
            else
            {
                $usr_name = $_SESSION['usr']['Mail'];
                echo '<a href="logout.php">logout</a> <br>';
                echo "$usr_name";
                
            }
        ?>
    </div>
</body>
</html>
