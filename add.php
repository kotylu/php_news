<?php
session_start();

            if (!isset($_SESSION['usr']))
            {
                header("Location: login.php");
                die();
            }
    require_once("dbcon.php");
    $db = new Database();
    require_once("categoryRepository.php");
    require_once("authorRepository.php");
    $cats = new CategoryRepository($db);
    $auths = new AuthorRepository($db);
    $cats_all = $cats->getAll();
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css" media="all">
</head>
<body>

    <nav>
        <a href="index.php">Zprávy</a>
        <a href="categories.php">Kategorie</a>
        <a href="authors.php">Autoři</a>
        <a href="admin.php">Administrace článků</a>
        <a href="add.php">Přidat článek</a>
    </nav>
    <section>
        <h1>Články</h1>
        <h4>přidej článek</h4>

            <form action="insert.php" method="post">
                <label for="art_cat">kagetorie: </label>
                <select id="cat" name="art_cat">
                    <?php
                        foreach($cats_all as $c)
                        {
                            $cid = $c['ID'];
                            $cname = $c['Name'];
                            echo "<option value=".'"'.$cid.'"'.">$cname</option>";
                        }
                    ?>
                </select>
                <br>
                <label for="art_title">
                    Nadpis: 
                    <input name="art_title" type="text" required></label>
                <br>
                    <textarea name="art_text" placeholder="Text článku" cols="30" rows="10" required></textarea>
                <br>
                <!-- <?php    echo "today: ".date("dmY");    ?> -->
                <br>
                <button type="submit"> ok</button>
            </form>
    </section>
    <div class="account">
        <?php
            if (!isset($_SESSION['usr']))
            {
                echo '<a href="login.php">login</a> <br>';
                echo '<a href="signup.php">signup</a>';
            }
            else
            {
                $usr_name = $_SESSION['usr']['Mail'];
                echo '<a href="logout.php">logout</a> <br>';
                echo "$usr_name";
                
            }
        ?>
    </div>
</body>
</html>
